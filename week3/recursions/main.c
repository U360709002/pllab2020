#include <stdio.h>
#include <stdlib.h>

int count_digit(int num)
{
   static int count=0;

   if(num > 0)
   {
       count++;
       count_digit(num/10);

   }
   else{
    return count;
   }

}

int main()
{
    int num = 1234656556;
    int result = count_digit(num);
    printf(" digits in %d is %d\n",num,result);
    return 0;
}
